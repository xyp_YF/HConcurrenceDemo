package com.xyp.demo.hconcurrence.remote;

import com.xyp.demo.hconcurrence.mode.StoreOrder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * @author xuyuanpeng
 * @version 1.0
 * @date 2019-05-15 14:28
 */
@Service
public class RemoteService {
    /**
     * 假装这是一个远程接口
     * @param code
     * @return
     */
    public StoreOrder getOrderByCode(String code) {
        StoreOrder order=new StoreOrder();
        order.setCreateTime(System.currentTimeMillis());
        order.setId(new Random().nextInt());
        order.setOrderCode(code);
        return order;
    }

    /**
     * 假装这是一个批量请求的远程接口
     * @param codes
     * @return
     */
    public List<StoreOrder> getOrderListByCodeBatch(List<String> codes){
        List<StoreOrder> list = new ArrayList<>();
        for(String code : codes){
            list.add(this.getOrderByCode(code));
        }
        return list;
    }
}
