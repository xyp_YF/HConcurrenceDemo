package com.xyp.demo.hconcurrence.service;

import com.xyp.demo.hconcurrence.mode.StoreOrder;

/**
 * @author xuyuanpeng
 * @version 1.0
 * @date 2019-05-14 14:34
 */
public interface IOrderService {
    public StoreOrder getOrderByCode(String code);
}
