package com.xyp.demo.hconcurrence.service;

import com.xyp.demo.hconcurrence.mode.StoreOrder;

import java.util.concurrent.ExecutionException;

/**
 * @author xuyuanpeng
 * @version 1.0
 * @date 2019-05-15 15:23
 */
public interface IPerformanceOrderService {
    public StoreOrder getOrderByCode(String code) throws ExecutionException, InterruptedException;
}
