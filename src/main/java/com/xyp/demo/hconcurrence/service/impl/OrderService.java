package com.xyp.demo.hconcurrence.service.impl;

import com.xyp.demo.hconcurrence.mode.StoreOrder;
import com.xyp.demo.hconcurrence.remote.RemoteService;
import com.xyp.demo.hconcurrence.service.IOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Random;

/**
 * @author xuyuanpeng
 * @version 1.0
 * @date 2019-05-14 14:34
 */
@Service
public class OrderService implements IOrderService {
    @Autowired
    private RemoteService remoteService;
    @Override
    public StoreOrder getOrderByCode(String code) {
        return remoteService.getOrderByCode(code);
    }
}
