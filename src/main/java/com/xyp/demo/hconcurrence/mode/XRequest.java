package com.xyp.demo.hconcurrence.mode;

import java.util.Map;
import java.util.concurrent.CompletableFuture;

/**
 * @author xuyuanpeng
 * @version 1.0
 * @date 2019-05-15 14:33
 */
public class XRequest {
    private Integer orderId;
    private String orderCode;
    private CompletableFuture<StoreOrder> future;

    public CompletableFuture<StoreOrder> getFuture() {
        return future;
    }

    public void setFuture(CompletableFuture<StoreOrder> future) {
        this.future = future;
    }

    public String getOrderCode() {
        return orderCode;
    }

    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }
}
