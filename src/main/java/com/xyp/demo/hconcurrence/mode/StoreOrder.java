package com.xyp.demo.hconcurrence.mode;

/**
 * @author xuyuanpeng
 * @version 1.0
 * @date 2019-05-14 14:35
 */
public class StoreOrder {
    public Integer id;
    public String orderCode;
    public Long createTime;
    public String createBy;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOrderCode() {
        return orderCode;
    }

    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }
}
