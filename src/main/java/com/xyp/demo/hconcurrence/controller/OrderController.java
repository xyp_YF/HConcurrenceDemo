package com.xyp.demo.hconcurrence.controller;

import com.xyp.demo.hconcurrence.mode.StoreOrder;
import com.xyp.demo.hconcurrence.service.IOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author xuyuanpeng
 * @version 1.0
 * @date 2019-05-14 14:31
 */
@RestController
@RequestMapping("/order")
public class OrderController {
    @Autowired
    private IOrderService orderService;

    @RequestMapping("/getOrderByCode")
    public StoreOrder getOrderByCode(String code){
        return orderService.getOrderByCode(code);
    }
}
