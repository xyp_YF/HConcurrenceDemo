import com.xyp.demo.hconcurrence.StartApplication;
import com.xyp.demo.hconcurrence.mode.StoreOrder;
import com.xyp.demo.hconcurrence.service.IOrderService;
import com.xyp.demo.hconcurrence.service.IPerformanceOrderService;
import com.xyp.demo.hconcurrence.utils.JsonMapper;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Random;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;

/**
 * @author xuyuanpeng
 * @version 1.0
 * @date 2019-05-14 14:43
 */
@SpringBootTest(classes = StartApplication.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class Test {
    @Autowired
    private IOrderService orderService;
    @Autowired
    private IPerformanceOrderService performanceOrderService;
    private static final Integer number=10000;

    private CountDownLatch cdl=new CountDownLatch(number);

    /**
     * 非并发
     */
    @org.junit.Test
    public void testThread(){
        //start 1000个线程，挨个执行
        long startTime=System.currentTimeMillis();
        for(int i=0;i<number;i++){
            Thread thread = new Thread(()->{
                StoreOrder order = orderService.getOrderByCode("12345");
//                System.out.println(JsonMapper.toJsonString(order));
            });
            thread.start();
        }
        long finshTime=System.currentTimeMillis()-startTime;
        System.out.println("处理完成时间："+finshTime);
        //避免以上不输出，直接down掉
        try {
            Thread.sleep(50000);
        }catch (Exception e){

        }
    }

    /**
     * 测试用例，不会等主线程执行。他执行完毕，就直接down掉了
     */
    @org.junit.Test
    public void testThread2(){
        //在线程中进行wait，当 cdl.countDown() 递减，直至=0是，wait完毕
        //这时创建的1000个线程，也创建完毕，1000个线程内容，执行同时执行

        long startTime=System.currentTimeMillis();

        for(int i=0;i<number;i++){
            Thread thread = new Thread(()->{
                try {
                    cdl.await();
                    StoreOrder order = orderService.getOrderByCode("CODE_"+new Random().nextInt());
//                    System.out.println(JsonMapper.toJsonString(order));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            });
            thread.start();
            cdl.countDown();
        }
        long finshTime=System.currentTimeMillis()-startTime;
        System.out.println("处理完成时间："+finshTime);
        //避免以上不输出，直接down掉
        try {
            Thread.sleep(50000);
        }catch (Exception e){

        }

    }


    @org.junit.Test
    public void testThread3(){
        //在线程中进行wait，当 cdl.countDown() 递减，直至=0是，wait完毕
        //这时创建的1000个线程，也创建完毕，1000个线程内容，执行同时执行

        long startTime=System.currentTimeMillis();

        for(int i=0;i<number;i++){
            Thread thread = new Thread(()->{
                try {
                    cdl.await();
                    StoreOrder order = performanceOrderService.getOrderByCode("CODE_"+new Random().nextInt());
//                    System.out.println("RESULT>>>"+JsonMapper.toJsonString(order));
                } catch (InterruptedException | ExecutionException e) {
                    e.printStackTrace();
                }
            });
            thread.start();
            cdl.countDown();
        }
        long finshTime=System.currentTimeMillis()-startTime;
        System.out.println("处理完成时间："+finshTime);
        //避免以上不输出，直接down掉
        try {
            Thread.sleep(50000);
        }catch (Exception e){
            // who care?
        }
    }



    @org.junit.Test
    public void test(){
        System.out.println("TEST");
        StoreOrder order=orderService.getOrderByCode("ABC");
        System.out.println(JsonMapper.toJsonString(order));
    }
}
